"use strict";

let gulp = require("gulp");
let sourcemaps = require("gulp-sourcemaps");
let del = require("del");
let runSequence = require("run-sequence");
let gutil = require("gulp-util");
let path = require("path");
let notifier = require("node-notifier");
let util = require("gulp-util");

let less = require("gulp-less");
let babel = require("gulp-babel");
let NwBuilder = require("node-webkit-builder");

let pjson = require("./package.json");

function standardHandler(err) {
    notifier.notify({message: "Error: " + err.message});
    util.log(util.colors.red("Error"), err.message);
    this.emit("end");
}

let paths = {
    nw: "webkitbuilds",
    dist: "dist",
    scripts: "./js/**/*.js",
    less: "./css/**/*.less",
    copies: ["./font/*.ttf", "./img/*.png", "./pages/*.html", "./package.json"]
};

gulp.task("clean", function(cb) {
    del([paths.dist + "/*", paths.nw], cb);
});

gulp.task("scripts", function() {
    return gulp.src(paths.scripts)
        .pipe(sourcemaps.init())
        .pipe(babel({
            blacklist: [
                "es3.memberExpressionLiterals",
                "es3.propertyLiterals",
                "es5.properties.mutators",
                "es6.arrowFunctions",
                "es6.blockScoping",
                "es6.classes",
                "es6.constants",
                "es6.destructuring",
                "es6.forOf",
                //"es6.modules",
                "es6.objectSuper",
                "es6.parameters.default",
                "es6.parameters.rest",
                "es6.properties.computed",
                "es6.properties.shorthand",
                "es6.regex.sticky",
                "es6.regex.unicode",
                "es6.spread",
                "es6.tailCall"
            ],
            optional: [
                "asyncToGenerator",
                //"minification.deadCodeElimination",
                "minification.inlineExpressions",
                "minification.memberExpressionLiterals",
                "minification.propertyLiterals",
                "utility.inlineEnvironmentVariables"
                //"validation.undeclaredVariableCheck"
            ]
        }))
        .on("error", standardHandler)
        //.pipe(concat("all.min.js"))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(path.join(paths.dist, "js")));
});

gulp.task("less", function() {
    return gulp.src(paths.less)
        .pipe(less())
        .pipe(gulp.dest(path.join(paths.dist, "css")));
});

gulp.task("bootstrap", function() {
    return gulp.src("./node_modules/bootstrap/dist/**", {base: "./node_modules/bootstrap/dist/"})
            .pipe(gulp.dest(path.join(paths.dist, "bootstrap")));
});

gulp.task("copies", function() {
    return gulp.src(paths.copies, {base: "."})
            .pipe(gulp.dest(paths.dist));
});

gulp.task("watch", function() {
    gulp.watch(paths.scripts, ["scripts"]);
    gulp.watch(paths.less, ["less"]);
    gulp.watch(paths.copies, ["copies"]);
});

gulp.task("nw", ["build"], function() {

    let dependencies = pjson.dependencies;
    let files = ["./dist/**"];
    for (let dependency in dependencies) {
        if (dependencies.hasOwnProperty(dependency)) {
            files.push("./node_modules/" + dependency + "/**");
        }
    }

    let nw = new NwBuilder({
        version: "0.12.2",
        platforms: ["linux64"],
        buildDir: paths.nw,
        files: files
    });

    nw.on("log", function(msg) {
        gutil.log("node-webkit-builder", msg);
    });

    return nw.build().catch(function(err) {
        gutil.log("node-webkit-builder", err);
    });
});

gulp.task("build", ["clean"], function(cb) {
    runSequence(["copies", "bootstrap", "scripts", "less"], cb);
});

gulp.task("default", ["build", "watch"]);
