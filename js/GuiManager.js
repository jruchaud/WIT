"use strict";

let gui = global.window.nwDispatcher.requireNwGui();
let player = require("./services/timer/PlayerService.js");
let activity = require("./services/activity/ActivityService.js");

class GuiManager {

    constructor() {
        this.createTray();

        this.popups = [];

        // Subscriptions to services
        player.on("idle", this.openWindow.bind(this, "idle.html"));
        activity.on("onActivityChange", this.openWindow.bind(this, "activityChange.html"));
    }

    createTray() {
        // Reference to window and tray
        let win = gui.Window.get();
        win.removeAllListeners("minimize");

        let tray;

        // Get the minimize event
        win.on("minimize", function() {
            tray = new gui.Tray({icon: "img/stopwatch-34108_640.png"});

            let menu = new gui.Menu();
            menu.append(new gui.MenuItem({label: "play"}));
            menu.append(new gui.MenuItem({label: "stop"}));
            tray.menu = menu;

            // Show window and remove tray when clicked
            tray.on("click", function() {
                win.show();
                this.remove();
                tray = null;
            });

            // Hide window
            this.hide();
        });

        win.on("focus", function() {
            if (tray) {
                tray.remove();
                tray = null;
            }
        });
    }

    // global.guiManager.openWindow("idle.html")
    openWindow(file) {
        let current = gui.Window.get();
        current.hide();

        activity.suspendActivityDetection();

        let window = gui.Window.open(file, {
            position: "center",
            focus: true,
            toolbar: false,
            frame: true,
            width: 480,
            height: 300
        });

        this.popups.push(window);

        window.on("close", this.restoreWindow.bind(this, current, window));
    }

    restoreWindow(win2Show, win2Hide) {
        win2Hide.hide(); // Pretend to be closed already
        win2Hide.close(true);

        this.popups.splice(this.popups.indexOf(win2Hide), 1);

        if (!this.popups.length) {
            win2Show.show();
        }
    }
}

module.exports = GuiManager;
