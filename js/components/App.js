"use strict";

let React = require("react");
let Routes = require("./router/Routes");
let Link = require("./router/Link");
let Timer = require("./timer/Timer");
let Report = require("./report/Report");
let Options = require("./options/Options");
let EditionLogs = require("./editing/EditionLogs");

class App extends React.Component {

   openReport() {
       this.refs.reportDropdown.getDOMNode().classList.toggle("open");
       this.refs.editionDropdown.getDOMNode().classList.remove("open");
   }

   openEdition() {
       this.refs.editionDropdown.getDOMNode().classList.toggle("open");
       this.refs.reportDropdown.getDOMNode().classList.remove("open");
   }

   closeAll() {
       this.refs.editionDropdown.getDOMNode().classList.remove("open");
       this.refs.reportDropdown.getDOMNode().classList.remove("open");
   }

  render() {
      return <div className="container-fluid">

            <div className="navbar navbar-default">
                <div className="navbar-header pull-left">
                  <a className="navbar-brand" href="#">WIT</a>
                </div>

                <div className="nav navbar-header pull-right">
                    <ul className="nav pull-right">
                        <li className="pull-left"><Link onClick={this.closeAll.bind(this)} to="Timer">Timer</Link></li>

                        <li ref="reportDropdown" className="dropdown pull-left">
                            <a className="dropdown-toggle" onClick={this.openReport.bind(this)}>
                                Report <span className="caret"></span>
                            </a>
                            <ul className="dropdown-menu">
                                <li><Link onClick={this.closeAll.bind(this)} to="Report">By date</Link></li>
                                <li><a href="#">By week</a></li>
                                <li><a href="#">By month</a></li>
                                <li><a href="#">By Year</a></li>
                            </ul>
                        </li>

                        <li ref="editionDropdown" className="dropdown pull-left">
                            <a className="dropdown-toggle" onClick={this.openEdition.bind(this)}>
                                Edition <span className="caret"></span>
                            </a>
                            <ul className="dropdown-menu">
                                <li><Link onClick={this.closeAll.bind(this)} to="EditionLogs">Logs</Link></li>
                                <li><a href="#">Tags</a></li>
                                <li><a href="#">Activities</a></li>
                            </ul>
                        </li>

                        <li className="pull-left"><Link onClick={this.closeAll.bind(this)} to="Options">Options</Link></li>
                    </ul>
                </div>
            </div>

            <Routes>
                <Timer />
                <Report />
                <Options />
                <EditionLogs />
            </Routes>
        </div>;
  }
}

module.exports = App;
