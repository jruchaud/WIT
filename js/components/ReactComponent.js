"use strict";

let React = require("react");

class ReactComponent extends React.Component {

    constructor(props) {
        super(props);

        // Force the binding otherwise when you use a function in the render
        // the function was bound on the window
        let proto = Object.getPrototypeOf(this);
        for (let attName in proto) {

            if (proto.hasOwnProperty(attName)) {
                let att = proto[attName];
                if (att instanceof Function) {
                    this[attName] = att.bind(this);
                }
            }
        }
    }
}

module.exports = ReactComponent;
