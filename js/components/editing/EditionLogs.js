"use strict";

let moment = require("moment");
let db = require("../../services/db/LogsDatabase");

let React = require("react");
let ReactComponent = require("../ReactComponent");
let FilterBar = require("../report/FilterBar");
let LogsTable = require("./LogsTable");

class EditionLogs extends ReactComponent {

    constructor(props) {
        super(props);
        this.state = {
            startDate: moment().subtract(1, "h"),
            endDate: moment(),
            result: null
       };
    }

    handleSearchResult(result, startDate, endDate) {
        this.setState({
            startDate: startDate,
            endDate: endDate,
            result: result
        });
    }

    onAdd() {

        let tags = this.refs.tags.getDOMNode().value.split(",");
        let startDate = moment(this.refs.beginDate.getDOMNode().value);
        let endDate = moment(this.refs.endDate.getDOMNode().value);

        return db.insertLog(tags, startDate, endDate);
    }

    render() {
        return <div>
                <h1>Search log</h1>
                <FilterBar key="edition" onSearchResult={this.handleSearchResult}/>
                <LogsTable data={this.state.result}/>

                <div>
                    <h1>Add log</h1>
                    <input ref="beginDate" type="datetime-local" className="form-control" required></input>
                    <input ref="endDate" type="datetime-local" className="form-control" required></input>
                    <input ref="tags" type="text" className="form-control" required placeholder="Enter tags with comma"></input>
                    <button onClick={this.onAdd} className="btn btn-primary btn-block">Add</button>
                </div>
            </div>;
    }
}

module.exports = EditionLogs;
