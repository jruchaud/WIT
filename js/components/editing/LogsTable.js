"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let db = require("../../services/db/LogsDatabase.js");
let moment = require("moment");

class LogsTable extends ReactComponent {

    onDelete(e) {
        let logId = e.target.dataset.id;
        if (logId) {
            db.deleteLog(logId);

            let index = parseInt(e.target.dataset.index, 10);
            this.props.data.splice(index, 1);
            this.forceUpdate();
        }
    }

    render() {
        let items = [];

        let classString = "";
        if (!this.props.data) {
            classString = "hidden";

        } else {
            this.props.data.forEach(function(log, index) {
                items.push(
                    <tr>
                        <td>{moment(log.startDate).format()}</td>
                        <td>{moment(log.endDate).format()}</td>
                        <td>{log.tags.join(", ")}</td>
                        <td>
                            <div ref="actions" className="btn-group" role="group">
                                <button type="button" className="btn btn-primary btn-lg" data-index={index} data-id={log._id}>
                                  <span className="glyphicon glyphicon-trash" data-index={index} data-id={log._id} aria-hidden="true"></span>
                                </button>
                            </div>
                        </td>
                    </tr>
                );
            }, this);
        }

        return <div className={classString}>
                <h2>Result</h2>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Date start</th>
                            <th>Date end</th>
                            <th>Tags</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody onClick={this.onDelete}>
                        {items}
                    </tbody>
                </table>

                <h3 className={items.length ? "hidden" : "text-center"}>
                    No result !
                </h3>
            </div>;
    }
}

module.exports = LogsTable;
