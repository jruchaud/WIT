"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let config = require("../../services/util/ConfigurationService");

class Options extends ReactComponent {

    setIdleTime(e) {
        config.setIdleTime(e.target.value);
    }

    setActivityTime(e) {
        config.setActivityTime(e.target.value);
    }

    setNoTimer(e) {
        config.setNoTimer(e.target.value);
    }

    setWeekDay(e) {
        config.setWeekDay(e.target.value);
    }

    render() {
        let idleTime = config.getIdleTime();
        let activityTime = config.getActivityTime();
        let noTimer = config.getNoTimer();
        let weekDay = config.getWeekDay();

        return <div className="form-horizontal">

                <div className="form-group">
                  <label for="idleTime" className="col-sm-2 control-label">Idle time (minutes)</label>
                  <div className="col-sm-10">
                    <input type="number" className="form-control" defaultValue={idleTime} min="0" step="1" onChange={this.setIdleTime}/>
                  </div>
                </div>

                <div className="form-group">
                  <label for="idleTime" className="col-sm-2 control-label">Activity time (minutes)</label>
                  <div className="col-sm-10">
                    <input type="number" className="form-control" id="activityTime" defaultValue={activityTime} min="0" step="1" onChange={this.setActivityTime}/>
                  </div>
                </div>

                <div className="form-group">
                  <label for="noTimer" className="col-sm-2 control-label">No timer (minutes)</label>
                  <div className="col-sm-10">
                    <input type="number" className="form-control" defaultValue={noTimer} min="0" step="1" onChange={this.setNoTimer}/>
                  </div>
                </div>

                <div className="form-group">
                    <label for="idleTime" className="col-sm-2 control-label">Week day</label>
                    <div className="col-sm-10">
                        <select name="select" className="form-control" id="weekDay" defaultValue={weekDay} onChange={this.setWeekDay}>
                            <option value="0">monday</option>
                            <option value="1">tuesday</option>
                            <option value="2">wednesday</option>
                            <option value="3">thursday</option>
                            <option value="4">friday</option>
                            <option value="5">saturday</option>
                            <option value="6">sunday</option>
                        </select>
                    </div>
                </div>
            </div>;
    }
}

module.exports = Options;
