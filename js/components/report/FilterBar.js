"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let moment = require("moment");
let db = require("../../services/db/LogsDatabase.js");

class FilterBar extends ReactComponent {

    componentDidMount() {
        let now = moment();
        now.add(1, "days");
        let endDefaultDate = now.format("YYYY-MM-DDThh:mm");

        now.subtract(7, "days");
        let startDefaultDate = now.format("YYYY-MM-DDThh:mm");

        this.refs.beginDate.getDOMNode().value = startDefaultDate;
        this.refs.endDate.getDOMNode().value = endDefaultDate;
    }

    search() {
        let startDate = moment(this.refs.beginDate.getDOMNode().value),
            endDate = moment(this.refs.endDate.getDOMNode().value);

        return db.searchLogs(null, startDate, endDate, true);
    }

    onSearch() {
        let onSearchResult = this.props.onSearchResult;
        this.search().then(onSearchResult);
    }

    render() {
        return <div>
                <input ref="beginDate" type="datetime-local" className="form-control" required></input>
                <input ref="endDate" type="datetime-local" className="form-control" required></input>
                <button onClick={this.onSearch} className="btn btn-primary btn-block">Search</button>
            </div>;
    }
}

module.exports = FilterBar;
