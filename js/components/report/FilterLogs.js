"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let nonce = require("nonce")();
let report = require("../../services/util/ExportService");
let Timeline = require("./Timeline");
let moment = require("moment");

class FilterLogs extends ReactComponent {

    constructor(props) {
        super(props);
        this.state = {data: null, tags: {}};
    }

    onClipboardCSV() {
        report.clipboard(report.CSV, this.state);
    }

    onClipboardTXT() {
        report.clipboard(report.TXT, this.state);
    }

    onMailCSV() {
        report.mail(report.CSV, this.state);
    }

    onMailTXT() {
        report.mail(report.TXT, this.state);
    }

    onShowSummary() {
        this.refs.summary.getDOMNode().classList.toggle("hidden");
    }

    onLogDetails(e) {
        let nodeId = e.target.dataset.id;
        if (nodeId) {
            this.refs[nodeId].getDOMNode().classList.toggle("hidden");
        }
    }

    onToggleFilterTag() {
        this.refs.filterTags.getDOMNode().classList.toggle("hidden");
    }

    componentWillReceiveProps(nextProps) {
        this.createTree(nextProps);
    }

    toggleTagChecked(e) {
        Object.keys(this.state.tags).forEach(function(tag) {
            this.state.tags[tag] = e.target.checked;
        }, this);

        this.createTree({
            data: this.props.data,
            tags: this.state.tags
        });
        e.stopPropagation();
    }

    filterTag(e) {
        this.state.tags[e.target.name] = e.target.checked;
        this.createTree({
            data: this.props.data,
            tags: this.state.tags
        });
    }

    createTree(currentState) {
        let data = {},
            tags = {};

        currentState.data.result.forEach(function(log) {
            let date = moment(log.startDate).format("MM/DD/YY");
            let diff = moment(moment(log.endDate).diff(log.startDate)).utcOffset(0);

            let current = data[date] = data[date] || {};
            log.tags.forEach(function(tag) {

                if (!currentState.tags || currentState.tags[tag]) {
                    current = current[tag] = current[tag] || {};
                }

                tags[tag] = true;
            });

            if (current.diff) {
                current.diff.add(diff);
            } else {
                current.diff = diff;
            }
            current.startDate = log.startDate;
            current.duration = current.diff.format("HH:mm:ss");
            current.endDate = log.endDate;
        });

        this.setState({
            data: data, tags: currentState.tags || tags,
            startDate: currentState.data.result.startDate,
            endDate: currentState.data.result.endDate
        });
    }

    renderTd(date, tags, tagObject) {
        let r = [];

        if (tagObject.duration) {
            let state = this.state;
            let filterTags = tags.filter(function(tag) {
                return state.tags[tag];
            });

            let key = nonce();

            if (filterTags.length) {
                r.push(
                    <tr>
                        <td>{date}</td>
                        <td>{tagObject.duration}</td>
                        <td>{filterTags.join(", ")}</td>
                        <td>
                            <button type="button" className="btn btn-default" data-id={key}>
                                <span data-id={key} className="glyphicon glyphicon-option-horizontal" aria-hidden="false"></span>
                            </button>
                        </td>
                    </tr>
                );
                r.push(
                    <tr ref={key} className="hidden">
                        <td colSpan="4"><Timeline data={tagObject} key={key}/></td>
                    </tr>
                );
            }
        }

        let keys = Object.keys(tagObject);
        for (let k of keys) {
            if (k !== "diff" && k !== "duration") {
                r = r.concat(this.renderTd(date, tags.concat(k), tagObject[k]));
            }
        }

        return r;
    }

    render() {
        let items = [];

        let classString = "";
        if (!this.state.data) {
            classString = "hidden";

        } else {
            let dates = Object.keys(this.state.data);
            for (let date of dates) {
                let r = this.renderTd(date, [], this.state.data[date]);
                items = items.concat(r);
            }
        }

        let tags = [];
        for (let tag of Object.keys(this.state.tags)) {
            tags.push(
                <div className="pull-left">
                    <label className="checkbox-label">
                        <input type="checkbox" name={tag} checked={this.state.tags[tag] ? "checked" : ""} />
                        {tag}
                    </label>
                </div>
            );
        }

        return <div className={classString}>
                <h2>Result</h2>

                <div ref="actions" className="btn-group" role="group">
                    <button type="button" className="btn btn-primary" onClick={this.onToggleFilterTag}>
                      Filter <span className="glyphicon glyphicon-tag" aria-hidden="true"></span>
                    </button>
                    <button type="button" className="btn btn-primary" onClick={this.onShowSummary}>
                      View <span className="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                    </button>
                    <button type="button" className="btn btn-primary" onClick={this.onClipboardCSV}>
                      CSV <span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
                    </button>
                    <button type="button" className="btn btn-primary" onClick={this.onClipboardTXT}>
                      TXT <span className="glyphicon glyphicon-copy" aria-hidden="true"></span>
                    </button>

                    <button type="button" className="btn btn-primary" onClick={this.onMailCSV}>
                      CSV <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                    </button>
                    <button type="button" className="btn btn-primary" onClick={this.onMailTXT}>
                      TXT <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                    </button>
                </div>

                <div ref="filterTags" className="hidden" onChange={this.filterTag}>
                    <div className="pull-left">
                        <label className="checkbox-label">
                            <input type="checkbox" defaultChecked onChange={this.toggleTagChecked} />
                            select/unselect all
                        </label>
                    </div>
                    {tags}
                </div>

                <div ref="summary" className="hidden">
                    <Timeline key={nonce()} data={this.state}/>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Duration</th>
                            <th>Tags</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody onClick={this.onLogDetails}>
                        {items}
                    </tbody>
                </table>

                <h3 className={items.length ? "hidden" : "text-center"}>
                    No result !
                </h3>
            </div>;
    }
}

module.exports = FilterLogs;
