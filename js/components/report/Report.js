"use strict";

let moment = require("moment");

let React = require("react");
let ReactComponent = require("../ReactComponent");
let FilterBar = require("./FilterBar");
let FilterLogs = require("./FilterLogs");

class Report extends ReactComponent {

    constructor(props) {
        super(props);

        this.state = {
            startDate: moment().subtract(1, "h"),
            endDate: moment(),
            result: []
       };
    }

    handleSearchResult(result) {
        this.setState({
            result: result
        });
    }

    render() {
        return <div>
                <h1>Search by date</h1>
                <FilterBar key="report" onSearchResult={this.handleSearchResult}/>
                <FilterLogs data={this.state}/>
            </div>;
    }
}

module.exports = Report;
