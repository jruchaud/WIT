"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let timelineService = require("../../services/activity/TimelineService.js");
let TimelineSession = require("./TimelineSession.js");
let moment = require("moment");
let nonce = require("nonce")();

class Timeline extends ReactComponent {

    constructor(props) {
        super(props);
        this.state = {sessions: []};
    }

    componentDidMount() {

        let startDate;
        if (this.props.data.startDate) {
            startDate = moment(this.props.data.startDate);
        }

        let endDate;
        if (this.props.data.endDate) {
            endDate = moment(this.props.data.endDate);
        }

        if (startDate && endDate) {
            let setState = this.setState.bind(this);

            timelineService.getSessions(startDate, endDate, true, true)
            .then(function(sessions) {
                setState({
                    sessions: sessions
                });
            });
        }
    }

    render() {
        let sessions = this.state.sessions;

        let items = [];

        // Create a wrapper for each window session

        let s, data;
        for (let i = 0, l = sessions.length; i < l; i++) {
            s = sessions[i];

            data = {
                title: s.name,
                color: s.color,
                width: s.ratio + "%"
            };

            items.push(<TimelineSession key={nonce()} data={data} />);

        }

        return <div className="timeline">
                <div className="sessions">
                    {items}
                </div>
            </div>;
    }
}

module.exports = Timeline;
