"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");

class TimelineSession extends ReactComponent {
    render() {
        let data = this.props.data;

        let title = data.title || "";
        let style = {
            background: data.color,
            width: data.width
        };
        return <div className="slice" style={style} title={title}>{title}</div>;
    }
}

module.exports = TimelineSession;
