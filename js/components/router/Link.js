"use strict";

let React = require("react");

class Link extends React.Component {

    constructor(props) {
        super(props);
    }

    goTo() {
        if (this.props.onClick) {
            this.props.onClick();
        }

        global.router.display(this.props.to);
    }

    render() {
        return <a href="#" onClick={this.goTo.bind(this)}>{this.props.children}</a>;
    }
}

module.exports = Link;
