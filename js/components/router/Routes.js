"use strict";

let React = require("react");

class Routes extends React.Component {

    constructor(props) {
        super(props);
        this.state = {index: 0};
        global.router = this;
    }

    display(to) {
        let children = this.props.children;

        for (let i = 0, l = children.length; i < l; i++) {
            let child = children[i];

            if (child.type.name === to) {
                this.setState({index: i});
                break;
            }
        }

    }

    getChild() {
        return this.props.children[this.state.index];
    }

    render() {
        return <div>
            {this.getChild()}
          </div>;
    }
}

module.exports = Routes;
