"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let player = require("../../services/timer/PlayerService");
let tags = require("../../services/timer/TagsService");

class ActionsBar extends ReactComponent {

    onPlay() {
        player.play();
    }

    onStop() {
        player.stop(true);
    }

    prevHistory() {
        player.stop();
        tags.prevHistory();
    }

    nextHistory() {
        player.stop();
        tags.nextHistory();
    }

    render() {
        return <div className="text-center">
                <div ref="actions" className="btn-group" role="group">
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.prevHistory}>
                      <span className="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
                    </button>
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.onPlay}>
                      <span className="glyphicon glyphicon-play" aria-hidden="true"></span>
                    </button>
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.onStop}>
                      <span className="glyphicon glyphicon-stop" aria-hidden="true"></span>
                    </button>
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.nextHistory}>
                      <span className="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
                    </button>
                </div>
           </div>;
    }
}

module.exports = ActionsBar;
