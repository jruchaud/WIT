"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let player = require("../../services/timer/PlayerService.js");
let tags = require("../../services/timer/TagsService.js");

class InputTag extends ReactComponent {

    constructor(props) {
        super(props);
        this.state = {options: [], value: ""};
    }

    getTags() {
        let setState = this.setState.bind(this);
        tags.getLastTags().then(function(labels) {
            setState({options: labels, value: ""});
        });
    }

    onKey(e) {
        let target = e.target;
        let value = target.value;

        if (e.keyCode === 13 && value !== "") { // On enter
            this.setState({options: this.state.options, value: ""});

            player.stop();
            tags.addTag(value);
        }
    }

    onChange(e) {
        this.setState({options: this.state.options, value: e.target.value});
    }

    render() {
        let options = [];

        if (this.state.value) {

            for (let i = 0, l = this.state.options.length; i < l; i++) {
                let item = this.state.options[i];

                if (options.length < 3 && item.indexOf(this.state.value) === 0) {
                    let element = <option>{item}</option>;
                    options.push(element);
                }
            }
        }

        return <div>
                <input id="search"className="form-control"
                    onKeyDown={this.onKey} onFocus={this.getTags}
                    onChange={this.onChange} placeholder="enter tag"
                    tabIndex="-1" autoComplete="off" autoCorrect="off" value={this.state.value}
                    autoCapitalize="off" spellCheck="false" role="textbox"
                    type="text" list="searchresults" />

                <datalist id="searchresults">
                {options}
                </datalist>
            </div>;
    }

}

module.exports = InputTag;
