"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let player = require("../../services/timer/PlayerService");
let tags = require("../../services/timer/TagsService");
let InputTag = require("./InputTag");

class Tags extends ReactComponent {

    componentDidMount() {
        this.forceUpdateBound = this.forceUpdate.bind(this);
        tags.on("tagsChanged", this.forceUpdateBound);
    }

    componentWillUnmount() {
        tags.removeListener("tagsChanged", this.forceUpdateBound);
    }

    onRemove(e) {
        let target = e.target;
        let index = target.dataset.index;

        player.stop();
        tags.removeTag(index);
    }

    render() {
        let onRemove = this.onRemove;

        let labels = tags.getTags();

        let rows = [];
        for (let i = 0; i < 5; i++) {
            let label = labels[i];

            rows.push(<li className="list-group-item">
                        <button type="button" className="close" aria-label="Close">
                            <span aria-hidden="true" onClick={onRemove} data-index={i}>&times;</span>
                        </button>

                        {label ? label : "empty"}
                    </li>
                 );
        }

        return <ul className="list-group">
                {rows}

                <li className="list-group-item">
                    <InputTag />
                </li>
            </ul>;
    }
}

module.exports = Tags;
