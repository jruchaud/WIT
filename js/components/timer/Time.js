"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let player = require("../../services/timer/PlayerService.js");

class Time extends ReactComponent {

    constructor(props) {
        super(props);

        this.state = {
            time: null,
            dayTime: null
        };
    }

    componentDidMount() {
        player.on("timeChanged", this.onTimeChanged);
        this.setDayTime();
    }

    onTimeChanged() {
        this.setState(function(previousState) {
            return {
                time: player.getTime(),
                dayTime: previousState.dayTime
            };
        });

        this.setDayTime();
    }

    setDayTime() {
        let setState = this.setState.bind(this);

        player.getDayTime()
        .then(function(dayTime) {
            setState({
                time: player.getTime(),
                dayTime: dayTime
            });
        });
    }

    componentWillUnmount() {
        player.removeListener("timeChanged", this.onTimeChanged);
    }

    onEdit() {
        this.refs.editButton.getDOMNode().classList.toggle("hidden");
        this.refs.editInput.getDOMNode().classList.toggle("hidden");
        this.refs.timeValue.getDOMNode().classList.toggle("hidden");

        let time = player.getTime() || "00:00:00";
        let editInputTime = this.refs.editInputTime.getDOMNode();
        editInputTime.value = time;
        editInputTime.max = time;
    }

    onSaveTime() {
        this.refs.editButton.getDOMNode().classList.toggle("hidden");
        this.refs.editInput.getDOMNode().classList.toggle("hidden");
        this.refs.timeValue.getDOMNode().classList.toggle("hidden");

        let editInputTime = this.refs.editInputTime.getDOMNode();

        player.removeTime(editInputTime.value);
    }

    render() {
        return <div>
                <button ref="editButton" type="button" className="btn btn-primary btn-small editButton" onClick={this.onEdit}>
                  <span className="glyphicon glyphicon-minus" aria-hidden="true"></span>
                </button>

                <div ref="editInput" className="input-group editInput hidden">

                    <input ref="editInputTime" type="time" className="form-control"
                        min="00:00:00" step="1"/>

                    <span className="input-group-btn">
                      <button className="btn btn-primary" type="button" onClick={this.onSaveTime}>Save</button>
                    </span>
                </div>

                <h1 ref="timeValue" className="digit">
                    {this.state.time ? this.state.time : "00:00:00"}
                </h1>

                <h5 className="digit">
                    {this.state.dayTime ? this.state.dayTime : "00:00:00"}
                </h5>
            </div>;
    }
}

module.exports = Time;
