"use strict";

let React = require("react");
let ReactComponent = require("../ReactComponent");
let Tags = require("./Tags");
let Time = require("./Time");
let ActionsBar = require("./ActionsBar");

class Timer extends ReactComponent {
    render() {
        return <div>
                <Time />
                <Tags />
                <ActionsBar />
            </div>;
    }
}

module.exports = Timer;
