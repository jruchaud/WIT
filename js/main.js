"use strict";

// Popup Idle
// Options with idle time + nb tags + Configuration week day
// Route
// Dropdown report week/day/month
// Alias
// Total time during day

let activity = require("./services/activity/ActivityService.js");
let noTimer = require("./services/timer/UseTheForceService.js");

noTimer.detect();

let GuiManager = require("./GuiManager");
global.guiManager = new GuiManager();

// React conf //
window.onload = function() {
    let React = require("react");
    let App = require("./components/App");

    React.render(<App/>, document.body);
};
