"use strict";

let Service = require("../util/Service");
let config = require("../util/ConfigurationService");
let sessions = require("./TimelineService");
let timer = require("../timer/TimerService");
let os = require("../util/OSService");

let moment = require("moment");

/**
 * This service handles the activity detection business logic
 */
class ActivityService extends Service {

    constructor() {
        super();

        this.activityChangeTimer = null;
        this.disableActivityDetection = false;

        os.on("activeWindowChange", this.addWindowSession.bind(this));
        os.on("windowTitleChange", this.updateWindowSession.bind(this));

        timer.on("timeChanged", this.onTimeChanged.bind(this));
    }

    /**
     * Create a new window session and trigger activity detection
     */
     addWindowSession(windowName) {
        this.setupActivityDetection(sessions.getCrtSession());

        sessions.createNewSession(windowName);
    };

    /**
     * Current active window is still the same
     * but it's title has changed.
     * Trace it by creating a new session with the same shape attributes than the previous one
     */
    updateWindowSession(windowName) {

        // Let's create a new session but with same color attributes than the last one,
        // since the active window has not really changed, only some of its attributes

        let crtSession = sessions.getCrtSession();
        if (crtSession && crtSession.name !== windowName) {
            sessions.createNewSession(windowName, crtSession.color);
        }
    }

    /**
     * If the user has stopped the timer while activity change detection is on going,
     * stop activity change detection process (means the user has handled it)
     */
    onTimeChanged() {
        if (!timer.getTime()) {
            clearTimeout(this.activityChangeTimer);
        }
    }

    /**
     * The currently active window has changed, let's check how long the previous window has been active for.
     * If the prev active window has been active longer than activity time duration (defined in conf) then
     * trigger a timer.
     * If the user stays again longer than activity time duration (defined in conf) on this newly active window
     * then trigger an alert.
     *
     * @param {Object} prevSession : previous active window session
     */
    setupActivityDetection(prevSession) {
        if (this.disableActivityDetection || !prevSession || !timer.getTime()) {

            clearTimeout(this.activityChangeTimer); // stop activity detection

        } else {

            let now = moment();
            if (now.diff(prevSession.startDate, "m") >= config.getActivityTime()) {

                // The user has stayed more than "config.getActivityTime()" minutes on a window
                // and now has just switched to another window.
                // If it stays at list as long as he did on the previous one, let's notify it to him

                this.activityChangeTimer = setTimeout(
                    this.emit.bind(this, "onActivityChange", now.toDate()),
                    moment.duration(config.getActivityTime(), "m").as("ms")
                );
                this.activityChangeTimer.crtActivityName = prevSession.name;
                this.activityChangeTimer.changeDetectedDate = now.toDate();

            } else if (prevSession.name === this.activityChangeTimer.crtActivityName) {

                // The user is back to the window from which we initiated the activity timer,
                // it seems to indicate that he is still working on the same activity. Let's clear the timer.

                clearTimeout(this.activityChangeTimer);
            }
        }
    }

    getActivityChangeDetectedDate() {
        return this.activityChangeTimer && this.activityChangeTimer.changeDetectedDate;
    }

    suspendActivityDetection() {
        this.disableActivityDetection = true;
        clearTimeout(this.activityChangeTimer);
    }

    resumeActivityDetection() {
        this.disableActivityDetection = false;
    }
}

module.exports = ActivityService.getInstance();
