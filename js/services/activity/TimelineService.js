"use strict";

let moment = require("moment");
let log = require("loglevel");
let db = require("../db/SessionsDatabase.js");

let Service = require("../util/Service");

/**
 * This service is meant to manage activity sessions
 * and keep track of the currently active session
 *
 * This service is mainly use by the timeline component,
 * which purpose is to give a graphic representation to the activity sessions
 **/
class TimelineService extends Service {

    constructor() {
        super();
        this.crtSession = null;
    }

    getCrtSession() {
        return this.crtSession;
    }

    setCrtSession(session) {
        this.crtSession = session;
    }

    createNewSession(windowName, color, startDate) {
        let now = moment();

        // Stop current session

        if (this.crtSession) {
            this.crtSession.endDate = now.valueOf();
            db.insertSession(this.crtSession);
        }

        // Check if a session corresponding to the given name already exists,
        // so that we can retrieve some properties

        let prev = db.getSessionByName(windowName);

        // Create new session

        this.crtSession = {
            name: windowName,
            color: color || prev && prev.color || "#" + (Math.floor(Math.random() * (999 - 100 + 1)) + 100),
            startDate: startDate || now.valueOf()
        };
    }

    /**
     * Retrieve window activity sessions for the period [startDAte; endDate]
     * If withEmptySession parameter equals true, then if there is a gap between 2 sessions in the results,
     * it will be filled with a fake session
     */
    getSessions(startDate, endDate, withEmptySession, normalize) {
        let rst;
        if (!startDate || !endDate) {

            log.error("[TimelineService] getSessions : parameter missing !");

        } else {
            let d1 = moment(startDate), d2 = moment(endDate);
            let concatCrtSession = this._concatCrtSession.bind(this);

            rst = db.getSessions(d1.toDate(), d2.toDate())
            .then(function(sessions) {

                // Append the current active session (if in the range [startDate;enDate])
                // to the sessions list result (because current active session is not persisted)
                // and process result conversion

                return this._convertSessionsResult(concatCrtSession(sessions, d2), d1, d2, withEmptySession, normalize);

            }.bind(this));
        }

        return rst;
    }

    _concatCrtSession(sessions, endDate) {
        // Clone crtSession to avoid modifications on it
        let crtSession = this.crtSession && JSON.parse(JSON.stringify(this.crtSession));

        return crtSession && endDate.isAfter(crtSession.startDate) ? sessions.concat([crtSession]) : sessions;
    }

    _convertSessionsResult(sessions, startDate, endDate, withEmptySession, normalize) {
        const MIN_RATIO = 1; // 1%

        let lastSessionEnd = startDate, timeRange = endDate.diff(startDate), s, ratio, prev;
        for (let i = 0; i < sessions.length; i++) {
            prev = sessions[i - 1];
            s = sessions[i];

            // Convert timestamp to moment

            s.startDate = moment(s.startDate);
            s.endDate = s.endDate ? moment(s.endDate) : endDate;

            // Normalize the session with startDate / endDate global range if normalized attribute = true
            // (truncate part of the sessions which are outside the global range, @see _normalize function)

            if (normalize) {
                this._normalize(s, startDate, endDate);
            }

            // Create an empty session when there is a significant hole between 2 sessions
            // else extend current session to fit the hole

            ratio = this._computeRatio(lastSessionEnd, s.startDate, timeRange);
            if (withEmptySession && ratio > 0) {
                if (ratio > MIN_RATIO) {
                    sessions.splice(i, 0, this._createFakeSession(lastSessionEnd, s.startDate, ratio));
                    i++;
                } else {
                    s.startDate = lastSessionEnd;
                    s.duration += ratio * timeRange / 100;
                    s.ratio += ratio;
                }
            }

            // If the previous session was very small according to the global time range,
            // let's try to group it with the current session

            if (prev && !prev.fake && prev.ratio < MIN_RATIO) {

                // Concatenate with next session
                prev.name += "/" + s.name;
                prev.duration += s.duration;
                prev.ratio += s.ratio;
                prev.endDate = s.endDate;
                prev.color = s.color;

                sessions.splice(i, 1); // remove current session from sessions list
                i--;
            }

            lastSessionEnd = s.endDate;
        }

        if (withEmptySession && lastSessionEnd.isBefore(endDate)) {
            sessions.push(this._createFakeSession(lastSessionEnd, endDate, this._computeRatio(lastSessionEnd, endDate, timeRange)));
        }

        // Adding startDate and endDate on sessions result object

        sessions.startDate = startDate;
        sessions.endDate = endDate;

        return sessions;
    }

    _computeRatio(startDate, endDate, timeRange) {
        let rst = 0;

        if (timeRange && startDate && endDate) {
            rst = endDate.diff(startDate) / timeRange * 100;
        }

        return rst;
    }

    /*
     * Normalize session dates and duration according to the given dates
     * If the given session starts before the given startDate or end after the given endDate,
     * this function will remove the session part outside of the scope
     */
    _normalize(session, startDate, endDate) {

        if (session.startDate.isBefore(startDate)) {
            session.startDate = startDate;
        }
        if (session.endDate.isAfter(endDate)) {
            session.endDate = endDate;
        }

        session.duration = session.endDate.diff(session.startDate);
        session.ratio = this._computeRatio(session.startDate, session.endDate, endDate.diff(startDate));

        return session;
    }

    _createFakeSession(startDate, endDate, ratio) {
        return {
            name: "",
            color: "transparent",
            startDate: startDate,
            endDate: endDate,
            duration: endDate.diff(startDate),
            ratio: ratio,
            fake: true
        };
    }
}

module.exports = TimelineService.getInstance();
