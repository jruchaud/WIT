"use strict";

let Datastore = require("nedb");
let moment = require("moment");
let pjson = require("../../../package.json");

let Service = require("../util/Service");

class LogsDatabase extends Service {

    constructor() {
        super();

        let dbFileNamePrefix = pjson.dbFileNamePrefix,
            path = (process.env.HOME || process.env.USERPROFILE) + "/.wit/";

        this.db = new Datastore({filename: path + dbFileNamePrefix + "logs.json", autoload: true});
    }

    insertLog(tags, startDate, endDate) {
        let m1 = startDate && moment(startDate),
            m2 = endDate && moment(endDate);

        let db = this.db;
        return new Promise(function(resolve, reject) {
            db.insert({
                tags: tags.sort(), // keep the key in same order to be able to create a tree
                startDate: m1.valueOf(),
                endDate: m2.valueOf()
            }, function(err, newDocs) {
                if (!err) {
                    resolve(newDocs);
                } else {
                    reject(err);
                }
            });
        });
    }

    /**
     * Search for logs in the database
     */
    searchLogs(tags, startDate, endDate, truncate) {
        let query = {};
        let t1 = startDate && moment(startDate).valueOf(),
            t2 = endDate && moment(endDate).valueOf();

        if (tags) { query.tags = {$in: tags}; }
        if (t1) { query.endDate = {$gte: t1}; }
        if (t2) { query.startDate = {$lte: t2}; }

        let db = this.db,
            normalize = this._normalize.bind(this);

        return new Promise(function(resolve, reject) {
            db.find(query).sort({startDate: -1}).exec(function(err, docs) {
                if (!err) {

                    // - tuncate periods to fit search dates params if required

                    if (truncate) {
                        docs.forEach(function(d) {
                            normalize(d, t1, t2);
                        });
                    }

                    docs.startDate = startDate;
                    docs.endDate = endDate;

                    resolve(docs);
                } else {
                    reject(err);
                }
            });
        });
    }

    getHistory(index) {
        let db = this.db;
        return new Promise(function(resolve, reject) {
            db.find({}).sort({startDate: -1}).skip(index).limit(1).exec(function(err, docs) {
                if (!err) {
                    resolve(docs);
                } else {
                    reject(err);
                }
            });
        });
    }

    getLastTags() {
        let db = this.db;
        return new Promise(function(resolve, reject) {
            db.find({}).sort({startDate: -1}).limit(100).exec(function(err, docs) {
                if (!err) {
                    let map = {};
                    docs.forEach(function(log) {
                        log.tags.forEach(function(tag) {
                            map[tag] = true;
                        });
                    });

                    resolve(Object.keys(map));
                } else {
                    reject(err);
                }
            });
        });
    }

    deleteLog(id) {
        let db = this.db;
        return new Promise(function(resolve, reject) {
            db.remove({_id: id}, {}, function(err) {
                if (!err) {
                    resolve();
                } else {
                    reject();
                }
            });
        });
    }

    _normalize(period, tstmpStart, tstmpEnd) {
        if (period.startDate < tstmpStart) {
            period.startDate = tstmpStart;
        }

        if (period.endDate > tstmpEnd) {
            period.endDate = tstmpEnd;
        }

        return period;
    };
}

module.exports = LogsDatabase.getInstance();
