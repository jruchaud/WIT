"use strict";

let Datastore = require("nedb");
let log = require("loglevel");
let pjson = require("../../../package.json");

let Service = require("../util/Service");

class SessionsDatabase extends Service {

    constructor() {
        super();

        let dbFileNamePrefix = pjson.dbFileNamePrefix,
            path = (process.env.HOME || process.env.USERPROFILE) + "/.wit/";

        this.db = new Datastore({filename: path + dbFileNamePrefix + "sessions.json", autoload: true});

        // Adding some indexes on the datastore to speed up queries

        let errorHandler = function(err) {
            if (err) {
                log.error("[SessionsDatabase] - An error occured while creating indexes : ", err);
            }
        };

        this.db.ensureIndex({fieldName: ["startDate", "endDate"]}, errorHandler);
        this.db.ensureIndex({fieldName: "name"}, errorHandler);
    }

    insertSession(session) {
        let db = this.db;
        return new Promise(function(resolve, reject) {
            db.insert(session,
            function(err, newDocs) {
                if (!err) {
                    resolve(newDocs);
                } else {
                    reject(err);
                }
            });
        });
    }

    getSessionByName(name) {
        let db = this.db;
        return new Promise(function(resolve, reject) {
            db.findOne({
                name: name
            }, function(err, docs) {
                if (!err) {
                    resolve(docs);
                } else {
                    reject(err);
                }
            });
        });
    }

    getSessions(startDate, endDate) {
        let query = {};

        if (endDate) { query.startDate = {$lte: endDate.getTime()}; }
        if (startDate) { query.endDate = {$gte: startDate.getTime()}; }

        let db = this.db;

        return new Promise(function(resolve, reject) {
            db.find(query).sort({startDate: 1}).exec(function(err, docs) {

                if (!err) {
                    resolve(docs);
                } else {
                    reject(err);
                }
            });
        });
    }
}

module.exports = SessionsDatabase.getInstance();
