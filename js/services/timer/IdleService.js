"use strict";

let Service = require("../util/Service");
let config = require("../util/ConfigurationService");
let os = require("../util/OSService");

let moment = require("moment");

class IdleService extends Service {

    constructor() {
        super();

        this.time = null;

        os.on("idleTime", this.onIdleTime.bind(this));
    }

    getTime() {
        return this.time;
    }

    onIdleTime(time) {
        let configIdleTime = config.getIdleTime();
        let durationIdleTime = moment.duration(configIdleTime, "m");

        if (time >= durationIdleTime.asMilliseconds() && !this.time) {
            this.setup();
        }
    }

    setup() {
        let configIdleTime = config.getIdleTime();
        let durationIdleTime = moment.duration(configIdleTime, "m");

        this.time = moment().subtract(durationIdleTime).toDate();
        this.emit("idle");
    }

    tearDown() {
        this.time = null;
    }
}

module.exports = IdleService.getInstance();
