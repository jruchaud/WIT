"use strict";

let Service = require("../util/Service");
let timer = require("./TimerService");
let idle = require("./IdleService");
let tags = require("./TagsService");
let logs = require("../db/LogsDatabase");

let moment = require("moment");

class PlayerService extends Service {

    constructor() {
        super();

        timer.on("timeChanged", this.emit.bind(this, "timeChanged"));
        idle.on("idle", this.onIdle.bind(this));
    }

    play() {
        idle.tearDown();
        timer.start();
    }

    pause() {
        let time = timer.getTime();

        if (time) {
            logs.insertLog(tags.getTags(), time, new Date());
            timer.stop();
        }
    }

    stop() {
        this.pause();
        timer.resume();
    }

    onIdle() {
        let time = timer.getTime();
        if (time) {
            this.emit("idle");
        }
    }

    keepIdle() {
        idle.tearDown();
    }

    removeIdle() {
        logs.insertLog(tags.getTags(), timer.getTime(), idle.getTime());

        timer.stop();
        idle.tearDown();

        this.play();
    }

    reassignIdle() {
        let time = timer.getTime();
        let idleTime = idle.getTime();

        logs.insertLog(tags.getTags(), time, idleTime);

        let diff = moment().diff(moment(idleTime));
        timer.suspend(diff);
        idle.tearDown();
    }

    removeTime(value) {
        let duration = timer.getSuspendDuration();
        if (duration) {
            let d = moment.duration(value);
            let value = moment.duration(duration).subtract(d);
            timer.suspend(value.asMilliseconds());
        }

        let time = timer.getTime();
        if (time) {
            let d = moment.duration(value);
            let endTime = moment().subtract(d).toDate();
            logs.insertLog(tags.getTags(), time, endTime);

            timer.suspend(d.asMilliseconds());
        }
    }

    getTime() {
        let time = timer.getTime();
        if (time) {
            let start = moment();
            let diff = moment(start.diff(time)).utcOffset(0);
            return diff.format("HH:mm:ss");
        }

        let duration = timer.getSuspendDuration();
        if (duration) {
            let diff = moment(duration).utcOffset(0);
            return diff.format("HH:mm:ss");
        }
    }

    getDayTime() {
        let startDate = moment().startOf("day");
        let endDate = moment().endOf("day");

        return logs.searchLogs(null, startDate, endDate)
                .then(function(results) {

                    let count = results.reduce(function(sum, log) {
                        let start = moment(log.startDate);
                        let end = moment(log.endDate);

                        return sum + end.diff(start);
                    }, 0);

                    let time = timer.getTime();
                    if (time) {
                        let now = moment();
                        count += now.diff(time);
                    }

                    let value = moment(count).utcOffset(0);
                    return value.format("HH:mm:ss");
                });
    }

}

module.exports = PlayerService.getInstance();
