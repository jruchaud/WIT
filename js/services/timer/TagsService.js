"use strict";

let Service = require("../util/Service");
let logs = require("../db/LogsDatabase");

class TagsService extends Service {

    constructor() {
        super();

        this.tags = [];
        this.historyIndex = 0;
    }

    getTags() {
        return this.tags;
    }

    addTag(tag) {
        this.tags.push(tag);
        this.emit("tagsChanged");
    }

    removeTag(index) {
        this.tags.splice(index, 1);
        this.emit("tagsChanged");
    }

    setTagsFromHistory() {
        logs.getHistory(this.historyIndex)
        .then(function(rows) {

            if (rows && rows.length) {
                let row = rows[0];
                this.tags = row.tags.slice(0);
            } else {
                this.tags = [];
            }
            this.emit("tagsChanged");
        }.bind(this));
    }

    prevHistory() {
        this.historyIndex++;
        this.setTagsFromHistory();
    }

    nextHistory() {
        if (this.historyIndex === -1) {
            return;
        }

        this.historyIndex--;
        this.setTagsFromHistory();
    }

    getLastTags() {
        return logs.getLastTags();
    }

}

module.exports = TagsService.getInstance();
