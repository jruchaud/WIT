"use strict";

let Service = require("../util/Service");
let moment = require("moment");

class TimerService extends Service {

    constructor() {
        super();

        this.time = null;
    }

    getTime() {
        return this.time;
    }

    getSuspendDuration() {
        return this.suspendDuration;
    }

    start() {
        this.time = new Date();
        this.resume();

        this.timeChange();
    }

    stop() {
        this.time = null;
        this.timeChange();
    }

    suspend(duration) {
        this.suspendDuration = duration;
        this.stop();
    }

    resume() {
        if (this.suspendDuration) {
            this.time = moment().subtract(this.suspendDuration, "ms").toDate();
            this.suspendDuration = null;
        }
    }

    timeChange() {
        clearTimeout(this.timeChangeTimer);

        this.emit("timeChanged");
        if (this.time) {
            this.timeChangeTimer = setTimeout(this.timeChange.bind(this), 1000);
        }
    }

}

module.exports = TimerService.getInstance();
