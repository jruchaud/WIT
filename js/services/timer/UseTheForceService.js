"use strict";

let Service = require("../util/Service.js");

let moment = require("moment");
let config = require("../util/ConfigurationService.js");
let timer = require("../timer/TimerService.js");

class UseTheForceService extends Service {

    constructor() {
        super();

        this.notStartedTimer = null;

        timer.on("timeChanged", this.onTimeChanged.bind(this));
    }

    onTimeChanged() {
        clearTimeout(this.notStartedTimer);
        if (!timer.getTime()) {
            this.detect();
        }
    }

    detect() {
        let noTimerConfig = config.getNoTimer();
        if (noTimerConfig) {

            let duration = moment.duration(noTimerConfig, "m");
            let detect = this.detect.bind(this);

            this.notStartedTimer = setTimeout(
                function() {
                    global.window.alert("Do you need wit ?");
                    detect();

                }, duration.asMilliseconds());
        }
    }
}

module.exports = UseTheForceService.getInstance();
