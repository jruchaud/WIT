"use strict";

let Service = require("./Service");

let DEFAULT_IDLE_TIME = 1; // 1 min
let DEFAULT_ACTIVITY_TIME = 1; // 1 min
let DEFAULT_NO_TIMER = 0; // desabled
let DEFAULT_WEEK_DAY = 0; // Monday

class ConfigurationService extends Service {

    // time as minutes
    setIdleTime(time) {
        localStorage.idleTime = time;
        this.emit("idleTimeChanged", time);
    }

    getIdleTime() {
        return parseInt(localStorage.idleTime || DEFAULT_IDLE_TIME, 10);
    }

    // time as minutes
    setActivityTime(time) {
        localStorage.activityTime = time;
        this.emit("activityTimeChanged", time);
    }

    getActivityTime() {
        return parseInt(localStorage.activityTime || DEFAULT_ACTIVITY_TIME, 10);
    }

    // 0 -> Monday
    setWeekDay(index) {
        localStorage.weekDay = index;
        this.emit("weekDayChanged", index);
    }

    getWeekDay() {
        return parseInt(localStorage.weekDay || DEFAULT_WEEK_DAY, 10);
    }

    setNoTimer(time) {
        localStorage.noTimer = time;
        this.emit("notTimerChanged", time);
    }

    getNoTimer() {
        return parseInt(localStorage.noTimer || DEFAULT_NO_TIMER, 10);
    }
}

module.exports = ConfigurationService.getInstance();
