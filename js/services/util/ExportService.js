"use strict";

let Service = require("../util/Service");
let gui = global.window.nwDispatcher.requireNwGui();

class ExportService extends Service {

    constructor() {
        super();

        this.contentFormatters = {
            csv: this._getCSV.bind(this),
            txt: this._getTXT.bind(this)
        };
    }

    get CSV() {
        return "csv";
    }

    get TXT() {
        return "txt";
    }

    _getCSV(logs) {
        let result = "";

        let dates = Object.keys(logs.data);
        for (let date of dates) {
            result += this._getCSVRow(date, logs.data[date], [], logs.tags);
        }

        return result;
    }

    _getCSVRow(date, tagsTree, tagsPath, availableTags) {
        let r = "";

        if (tagsTree.duration) {
            let filterTags = tagsPath.filter(function(tag) {
                return availableTags[tag];
            });

            if (filterTags.length) {
                r += date + ";" + tagsTree.duration + ";" + filterTags.join(";") + ";\n";
            }
        }

        let keys = Object.keys(tagsTree);
        for (let k of keys) {
            if (k !== "diff" && k !== "duration") {
                r += this._getCSVRow(date, tagsTree[k], tagsPath.concat(k), availableTags);
            }
        }

        return r;
    }

    _getTXT(logs) {
        let result = "";

        let dates = Object.keys(logs.data);
        for (let date of dates) {
            result += date + "\n--------\n";
            result += "\n" + this._getTXTRow(logs.data[date], [], logs.tags) + "\n";
        }

        return result;
    }

    _getTXTRow(tagsTree, tagsPath, availableTags) {
        let r = "";

        if (tagsTree.duration) {
            let filterTags = tagsPath.filter(function(tag) {
                return availableTags[tag];
            });

            if (filterTags.length) {
                r += tagsTree.duration + " - " + filterTags.join(", ") + "\n";
            }
        }

        let keys = Object.keys(tagsTree);
        for (let k of keys) {
            if (k !== "diff" && k !== "duration") {
                r += this._getTXTRow(tagsTree[k], tagsPath.concat(k), availableTags);
            }
        }

        return r;
    }

    clipboard(format, logs) {
        let formatter = this.contentFormatters[format];

        if (formatter) {
            let content = formatter(logs);
            let clipboard = gui.Clipboard.get();

            clipboard.set(content, "text");
        }
    }

    mail(format, logs) {
        let formatter = this.contentFormatters[format];

        if (formatter) {
            let content = formatter(logs);
            content = content.replace(/\n/g, "%0A").replace(/;/g, "%3B").replace(/-/g, "%2D");

            let link = "mailto:?body=" + content;
            window.location.href = link;
        }
    }
}

module.exports = ExportService.getInstance();
