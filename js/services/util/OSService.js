"use strict";

let Service = require("./Service");
let x11 = require("x11");
let xprop = require("xprop");
let log = require("loglevel");

class OSService extends Service {

    constructor() {
        super();

        this.screenSaverInfo();
        this.monitorX11Activity();
    }

    screenSaverInfo() {
        let emit = this.emit.bind(this, "idleTime");

        x11.createClient(function(err0, display) {
            if (err0) {
                log.error(err0);
                return;
            }

            let X = display.client;
            X.require("screen-saver", function(err1, SS) {

                let queryIdle = function() {
                    SS.QueryInfo(display.screen[0].root, function(err2, info) {
                        emit(info.idle);
                        setTimeout(queryIdle, 1000);
                    });
                };

                queryIdle();
            });

            X.on("error", log.error);
        });
    }

    monitorX11Activity() {
        let detectActiveWindowChange = this.detectActiveWindowChange.bind(this);

        x11.createClient(function(err, display) {

            if (err) {

                log.error("[OSService] - An error occured : ", err);

            } else {
                let xClient = display.client;

                xClient.ChangeWindowAttributes(display.screen[0].root, {
                    eventMask: x11.eventMask.PropertyChange
                });

                // Listen to client display events to identify when the currently active window has change
                xClient.on("event", detectActiveWindowChange.bind(null, xClient));
                xClient.on("error", log.error);
            }
        });
    }

    detectActiveWindowChange(xClient, ev) {

        if (ev.name === "PropertyNotify") {

            xClient.GetAtomName(ev.atom, function(err, name) {

                // if the property change refers to the currently active window, retrieve new active window name

                if (name === "_NET_ACTIVE_WINDOW") {

                    // Our x client only listen for _NET_ACTIVE_WINDOW property change
                    // To be able to also listen for _NET_WM_NAME (window title change),
                    // we need to register a listener on this newly active window

                    this.registerWindowEvents(xClient, ev)
                    .then(this.getWindowName.bind(this))
                    .then(this.emit.bind(this, "activeWindowChange"));

                } else if (name === "_NET_WM_NAME") {

                    this.getWindowName(ev.wid)
                    .then(this.emit.bind(this, "windowTitleChange"));

                }
            }.bind(this));
        }
    }

    /**
     * x11 can detect when the _NET_ACTIVE_WINDOW (currently active window) has changed
     * However to be able to notice the change within the active window (such as title change),
     * we need to register a listener on that particlar window. Here, we tell to our x client to listen for property change
     * on the currently active window. Each time a property will change on that window, we will receive a new event on our x client.
     *
     * @param {Object} xClient : the x client object instanciated via x11
     * @param {Object} ev : event receive from our x client on active window change. This event contains the parent window id on which is attached
     *                    the new active window. We will use it to retrive the window id of the newly active window so that we can then register
     *                    property change on that particular window.
     */
    registerWindowEvents(xClient, ev) {
        return this.getActiveWindowId(xClient, ev)
        .then(function(wid) {
            xClient.ChangeWindowAttributes(wid, {
                eventMask: x11.eventMask.PropertyChange
            });

            return wid;
        });
    }

    /**
     * Retrieve the window id of the currently active window
     * when receiveing the _NET_ACTIVE_WINDOW property change event
     *
     * @param   {Object}   xClient
     * @param   {Object}   ev      Active window change event
     * @returns {Promise} resolved with the new active window id
     */
    getActiveWindowId(xClient, ev) {
        return new Promise(function(resolve, reject) {

            if (!ev.wid) {
                reject();
            } else {

                xClient.GetProperty(0, ev.wid, ev.atom, xClient.atoms.WINDOW, 0, 4, function(err, prop) {
                    if (err || !prop.data.length) {
                        log.warn("[OSService] - Couldn't retrieve active window property");
                        reject();

                    } else {
                        let wid = prop.data.readUInt32LE(0);

                        if (wid) {
                            resolve(wid);

                        } else {
                            log.warn("[OSService] - Couldn't retrieve active window property");
                            reject();
                        }
                    }
                });
            }

        });
    }

    /**
     * Retrieve the title of a window from a window id
     * @param   {number} windowId : window id
     * @returns {Promise} resolved with the window title
     */
    getWindowName(windowId) {
        return new Promise(function(resolve, reject) {

            if (!windowId) {
                reject();
            } else {

                xprop({prop: "_NET_WM_NAME", id: windowId}, function(err, properties) {

                    if (err) {
                        log.warn("[OSService] - Couldn't retrieve window name for id :", windowId);
                        reject();

                    } else {
                        resolve(properties.length && properties[0].value);
                    }
                });
            }
        });
    }

}

module.exports = OSService.getInstance();
