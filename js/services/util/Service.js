"use strict";

let events = require("events");

class Service extends events.EventEmitter {

    static getInstance() {
        if (!this.instance) {
            this.instance = new this();
        }
        return this.instance;
    }

}

module.exports = Service;
